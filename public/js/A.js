$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if(scroll < 81){
        $('#scrollHeader').hide().removeClass('sticky')
        $('#sidemenu').show(200)
    } else if(scroll > 5){
        $('#scrollHeader').addClass('sticky').fadeIn(500)
        $('#sidemenu').hide(100)
    }
});

$(document).ready(function() {
    $('.burguer-menu').click(function(){
        $(this).toggleClass('open');
    });
    $('#colec').hover(()=>{
        $("#menucolec").stop( false, true ).slideToggle();
    })
    $("#menucolec").hover(()=>{
        $("#menucolec").stop(true,false).slideToggle()
    })
});

