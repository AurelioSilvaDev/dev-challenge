# Desafio FrontEnd

## Instruções para o teste

* Dê um fork neste projeto;

* Desenvolva as telas;

* Atualize o readme com as instruções necessárias para rodar o seu código;

* Faça um pull request.

##### Sugestões de implementação

* Interação com JSON para renderizar os produtos (você vai encontrar um mockup em src/data/products.json)

* Filtro de produtos funcional

* Adicionar produtos ao carrinho

* Botão de carregar mais produtos

  
##### Dicas

* Evite usar linguagens, ferramentas e metodologias que não domine;

* Não esqueça de manter o package atualizado com os módulos necessários para rodar seu projeto;

  
###### Dúvidas: vinicius.diniz@somagrupo.com.br


#
### Estruturação:
* O app foi construido em PUG e Express utillizando **Node**. Suas dependências de execução são os próprios ***PUG***, ***Express*** e mais ***Bootstrap*** para estilização e responsividade.
 
* Este app foi desenvolvido de forma simples e dinâmica, utilizando *rotas* e o próprio ***PRODUCTS.JSON*** de mockup ***(provido no diretório src/data)*** como fonte de dados. 
* O layout foi feito em **PUG** e **Bootstrap** para melhor compatibilidade com diversos dispositivos, respeitando as definições de design ***(providos no diretório src/layout).***

### Requierimentos e dependências:
* É necessário  [**Node v12**](https://nodejs.org/en/) ou superior.
* Para instalar as dependências basta acessar a pasta raiz, após clonar o repositório e rodar o comando:
	
	## `npm i `

* Após a instalação de todas as dependências, ainda na pasta raiz, rode o comando:
	
	## `npm start`
	
* O app roda em um servidor **Express** no endereço local na porta **9876** *(localhost:9876)*.


#
##### [Aurelio Miguel da Rocha Silva]([https://bitbucket.org/AurelioSilvaDev/](https://bitbucket.org/AurelioSilvaDev/))
