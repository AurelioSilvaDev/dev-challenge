const express = require('express');
const app = express();
const path = require("path");
const helper = require("./helper.js");
const items = require("./data/products.json")
var opn = require('opn');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, "public")));

// Routes
// Route Home
app.get('/', (req, res)=>{
    res.render("index", {
        "year": helper.year
    });
});

app.get('/colecao/:id', (req, res)=>{
    console.log(req.params.id+"/")
    let el=[];
    for (const key in items) {
        if (items.hasOwnProperty(key)) {
            const e = items[key];
            if (e.categories.length > 3 && e.categoriesIds.length > 3){
                if (req.params.id+"/" == e.categories[1].split('/COLEÇÃO/')[1]) {
                    el.push(e);
                }
            } else if(e.categories.length < 3 && e.categoriesIds.length < 3){
                if (req.params.id+"/" == e.categories[0].split('/COLEÇÃO/')[1]) {
                    el.push(e);
                }
            }
            
        }
    }
    if(el.length > 0){
        res.render("item",{
            "produtos": el,
            "year": helper.year
        });
    } else {
        res.render("notfound",{
            "year": helper.year
        });
    }
})

// Route single page
app.get('/item/:id', (req, res)=>{
    for (const key in items) {
        if (items.hasOwnProperty(key)) {
            const el = items[key];
            if (el.productId == req.params.id) {
                res.render("items_page",{
                    "item": el,
                    "year": helper.year
                });
            }
        }
    }
})


// Start express server
app.listen(9876, ()=>{
    // opens the url in the default browser 
    // opn('http://localhost:9876', {app: 'chrome'});
});